package com.shahsk;

public class Consumer extends Thread {

    private DataQueue buffer;

    public Consumer(DataQueue buffer, String name) {
        this.buffer = buffer;
        setName(name);
        System.out.println("Consumer: Initialized");
    }

    @Override
    public void run() {
        while (true) {
            try {
                sleep(2500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            buffer.consume(getName());
        }
    }
}
