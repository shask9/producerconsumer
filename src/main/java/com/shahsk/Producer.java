package com.shahsk;

import java.util.Random;

public class Producer extends Thread {

    private DataQueue buffer;

    public Producer(DataQueue buffer, String name) {
        this.buffer = buffer;
        setName(name);
        System.out.println("Producer: Initialized");
    }

    @Override
    public void run() {
        while(true) {
            try {
                sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Random random = new Random();
            int[] data = {random.nextInt(100000),
                    random.nextInt(100000),
                    random.nextInt(100000),
                    random.nextInt(100000),
                    random.nextInt(100000)
            };
            buffer.produce(data,getName());
        }
    }
}
