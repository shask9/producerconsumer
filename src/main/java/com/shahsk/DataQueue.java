package com.shahsk;

import java.util.LinkedList;

public class DataQueue {

    private LinkedList<Integer> buffer;
    private int capacity;
    private int size = 0;

    public DataQueue(int capacity) {
        this.capacity = capacity;
        buffer = new LinkedList<>();
    }

    private boolean isFull() {
        return size == capacity;
    }

    private boolean isEmpty() {
        return size == 0;
    }

    protected synchronized void produce(int[] numbers, String name) {
        if(isFull()) {
            System.out.println("Producer Waiting...");
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Producer: Generating Data");
        for(int data: numbers) {
            buffer.offer(data);
            size++;
            System.out.println("Produced: " + data);
            if(isFull())
                break;
        }
        notifyAll();
    }

    protected synchronized int consume(String name) {
        if(isEmpty()) {
            System.out.println("Consumer Waiting...");
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.print("Consumer: Consuming ");
        int data = buffer.poll();
        size--;
        notifyAll();
        System.out.println(data);

        return data;
    }
}