package com.shahsk;

public class App {
    public static void main( String[] args ) {

        DataQueue buffer = new DataQueue(10);

        Producer p = new Producer(buffer,"p");
        Consumer c = new Consumer(buffer,"c");

        p.start();
        c.start();

    }
}
